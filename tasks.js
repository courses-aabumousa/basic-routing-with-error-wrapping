import { Router } from "express";
import createHttpError from "http-errors";

const tasksRouter = Router();

let tasks = [];
let lastIndex = 0;

tasksRouter.post("", (req, res) => {
    const { name, priority } = req.body;
  
    if (
      !name ||
      typeof name !== "string" ||
      !priority ||
      typeof priority !== "number" ||
      priority < 1 ||
      priority > 5
    ) {
      throw createHttpError.UnprocessableEntity('invalid task data');
    }
  
    const id = lastIndex++ + 1; // Generate a unique id
  
    // Create a new task object
    const newTask = {
      id,
      name,
      priority,
    };
  
    // Add the task to the tasks array
    tasks.push(newTask);
  
    res.status(201).json(newTask);
  });
  
  // PUT /tasks/:id endpoint
  tasksRouter.put("/:id", (req, res) => {
    const taskId = parseInt(req.params.id);
    const { name, priority } = req.body;
  
    if (
      !name ||
      typeof name !== "string" ||
      !priority ||
      typeof priority !== "number" ||
      priority < 1 ||
      priority > 5
    ) {
      throw createHttpError.UnprocessableEntity('invalid task data');
    }
    // Find the task in the tasks array by id
    const taskToUpdate = tasks.find((task) => task.id === taskId);
  
    if (!taskToUpdate) {
      throw createHttpError.NotFound('Task not found');
    }
  
    // Update the task with the new data
    taskToUpdate.name = name;
    taskToUpdate.priority = priority;
  
    res.status(200).json(taskToUpdate);
  });
  
  // GET /tasks endpoint
  tasksRouter.get("", (req, res) => {
    res.json(tasks);
  });
  
  // GET /tasks/:id endpoint
  tasksRouter.get("/:id", (req, res) => {
    const taskId = parseInt(req.params.id);
  
    // Find the task in the tasks array by id
    const task = tasks.find((task) => task.id === taskId);
  
    if (!task) {
      throw createHttpError.NotFound('Task not found');
    }
  
    res.json(task);
  });
  
  // DELETE /tasks/:id endpoint
  tasksRouter.delete("/:id", (req, res) => {
    const taskId = parseInt(req.params.id);
  
    // Find the index of the task in the tasks array by id
    const taskIndex = tasks.findIndex((task) => task.id === taskId);
  
    if (taskIndex === -1) {
      throw createHttpError.NotFound('Task not found');
    }
  
    // Remove the task from the tasks array
    const deletedTask = tasks.splice(taskIndex, 1)[0];
  
    res.json(deletedTask);
  });
  

export default tasksRouter;